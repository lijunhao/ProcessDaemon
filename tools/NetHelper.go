package tools

import (
	"fmt"
	"net"
	"os"
	"strings"
)

type NetHelper struct {
}

//region 获取本机IP，根据传入的IP前缀进行匹配
func (this *NetHelper) GetIntranetIp(ipprefix string) string {
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, address := range addrs {
		// 检查ip地址判断是否回环地址
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil && strings.HasPrefix(ipnet.IP.String(), ipprefix) {
				return ipnet.IP.String()
			}
		}
	}
	return ""
}

//endregion
