package main

import (
	"fmt"
	"time"
)

var processTitle1 = "测试服务-A"

func main() {
	for {
		fmt.Println(fmt.Sprintf("processTitle：%s，时间：%s", processTitle1, time.Now().Format("2006-01-02 15:04:05")))
		time.Sleep(30 * time.Second)
	}
}
