package main

import (
	"fmt"
	"time"
)

var processTitle2 = "测试服务-B"

func main() {
	for {
		fmt.Println(fmt.Sprintf("processTitle：%s，时间：%s", processTitle2, time.Now().Format("2006-01-02 15:04:05")))
		time.Sleep(30 * time.Second)
	}
}
