package daemonlibrary

type ServerModel struct {
	ServerIp    string
	ProcessList []ProcessModel
}

type ProcessModel struct {
	Title     string
	CheckName string
	StartCmd  string
}
